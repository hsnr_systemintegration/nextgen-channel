//#######################################################
// ### General ChannelJS call for SMART DATA EXCHANGE ###
//####################################################### 

// ### DB Config ###
var config = {
    db: {
        driverClass: "com.mysql.jdbc.Driver",
        connectionString: "jdbc:mysql://192.168.2.116:3306/openemr",
        username: "fhiruser",
        password: "fhir"
    }
}
exec(config);

/**
	The Main function for initialization

	@param {JSON} config - the config containing the DB config
*/
var exec = function (config) {
    try {
        var fhirConfig = initFHIR();
        writeData(config, fhirConfig);

        return writeResponse();
    } catch (e) {
        logger.error("An error occurred while processing: " + e);
        return createOperationOutcome('error', 'transient', e, 500, e);
    }
}

/**
	Initializes the FHIR obj
	
	@return {JSON} fhirConfig - the fhirConfig containing relevant data for FHIR interaction
*/
function initFHIR() {
    var fhirType = $('fhirType').toLowerCase();
    var fhirId = UUIDGenerator.getUUID();
    var versionId = 1;
    var data = AttachmentUtil.reAttachMessage(connectorMessage);
    var contentType = FhirUtil.getMIMETypeXML();
    var preferReturn = getPreferValue();

    var resource = FhirUtil.fromXML(data);
    var resourceType = resource.getResourceType();

    if (resourceType == null) {
        logger.warn("Resource type unknown, cannot be created as a(n) " + $("fhirType") + " resource.")
        return createOperationOutcome("error", "invalid", "Resource type unknown, cannot be created as a(n) " + $("fhirType") + " resource.");
    } else if (resourceType.toString().toLowerCase() != fhirType) {
        logger.warn("Resource type " + resourceType + " cannot be created as a(n) " + $("fhirType") + " resource.");
        return createOperationOutcome("error", "invalid", "Resource type " + resourceType + " cannot be created as a(n) " + $("fhirType") + " resource.");
    }
    var lastUpdated = updateResourceMeta(resource, fhirId, versionId);

    return {
        "fhirType": fhirType,
        "fhirId": fhirId,
        "versionId": versionId,
        "data": data,
        "contentType": contentType,
        "preferReturn": preferReturn,
        "resource": resource,
        "lastUpdated": lastUpdated
    }
}

/**
	Writes the observation resource data into an OpenEMR DB

	@param {JSON} config - the config containing the DB config
	@param {JSON} fhirConfig - the fhirConfig containing relevant data for FHIR interaction
*/
function writeData(config, fhirConfig) {
    var dbConn;
    var pubpid = fhirConfig.resource.getSubject().getReference();
    logger.info("Public Id for interaction is " + pubpid);
    var pulse = fhirConfig.resource.getValueQuantity().getValue();
    var steps = fhirConfig.resource.getComment();
    var effectiveDateTimeString = extractDateTime(fhirConfig);

    try {
        dbConn = DatabaseConnectionFactory.createDatabaseConnection(config.db.driverClass, config.db.connectionString, config.db.username, config.db.password);
        logger.info("Connect to DB: " + config.db.connectionString);

        var pid = getId(dbConn, "pid", "patient_data", "WHERE pubpid=" + pubpid);

        var encounterId = execEncounter(dbConn, pid, effectiveDateTimeString);
        execVitals(dbConn, pid, encounterId, effectiveDateTimeString, pulse, steps);
    } finally {
        if (dbConn) {
            dbConn.close();
            logger.info("Close DB connection: " + config.db.connectionString);
        }
    }
}

/**
	Converts the EffectiveDateTimeType of the observation resource to an DateTime string

	@param {JSON} fhirConfig - the fhirConfig containing relevant data for FHIR interaction
	
	@return {String} return the dateTime string
*/
function extractDateTime(fhirConfig) {
    var effectiveDateTimeString = "";
    if (fhirConfig.resource.hasEffectiveDateTimeType()) {
        var effectiveDateTime = fhirConfig.resource.getEffective();

        var replacements = {
            "%DAY%": effectiveDateTime.getDay(),
            "%MONTH%": effectiveDateTime.getMonth() + 1, // There is an Bug in the HAPI API. So the month has to be incremented once.
            "%YEAR%": effectiveDateTime.getYear(),
            "%HOUR%": effectiveDateTime.getHour(),
            "%MINUTE%": effectiveDateTime.getMinute()
        };

        effectiveDateTimeString = '%YEAR%-%MONTH%-%DAY% %HOUR%:%MINUTE%';
        effectiveDateTimeString = effectiveDateTimeString.replace(/%\w+%/g, function (all) {
            return replacements[all] || all;
        });
    } else {
        logger.warn("Resource has no effectiveDateTime");
    }
    
    return effectiveDateTimeString;
}

/**
	Get an Id stored in the OpenEMR DB
	
	@param {DatabaseConnection} dbconn - the DB connection obj
	@param {String} fieldName - the name of the field that contains the id
	@param {String} tableName - the name of the table the Id shall be extracted of
	@param {String} extension - additional sql query
	
	@return {Integer} return the Id
*/
function getId(dbConn, fieldName, tableName, extension) {
    var rs_id = dbConn.executeCachedQuery("SELECT " + fieldName + " FROM " + tableName + " " + extension);
    var id = 0;

    while (rs_id.next()) {
        id = rs_id.getInt(fieldName);
    }

    return id;
}


/**
	Executes the encounter creation
	
	@param {DatabaseConnection} dbconn - the DB connection obj
	@param {Integer} pid - the patient Id
	@param {String} effectiveDateTimeString - the dateTime string when the observation was created
	
	@return {Integer} return the id of the new created encounter
*/
function execEncounter(dbConn, pid, effectiveDateTimeString) {
    var encounterId = getId(dbConn, "encounter", "form_encounter", "ORDER BY id DESC LIMIT 1;") + 1;
    logger.info("New encounter has id " + encounterId);
    createEncounter(dbConn, pid, encounterId, effectiveDateTimeString);
    var lastEncounterFormId = getId(dbConn, "id", "form_encounter", "ORDER BY id DESC LIMIT 1;");
    registerEncounter(dbConn, pid, encounterId, effectiveDateTimeString, lastEncounterFormId);

    return encounterId;
}

/**
	Inserts a new encounter obj into the OpenEMR DB

	@param {DatabaseConnection} dbconn - the DB connection obj
	@param {Integer} pid - the patient Id
	@param {Integer} encounterId - the encounter Id
	@param {String} effectiveDateTimeString - the dateTime string when the observation was created
*/
function createEncounter(dbConn, pid, encounterId, effectiveDateTimeString) {
    dbConn.executeUpdate("INSERT INTO form_encounter (date, reason, facility, facility_id, pid, encounter,onset_date,sensitivity, pc_catid, provider_id, billing_facility) VALUES (STR_TO_DATE('" + effectiveDateTimeString + "', '%Y-%m-%e %H:%i:%s'), 'Sent by SMART DATA EXCHANGE!', 'Musterklinik', 3, " + pid + ", " + encounterId + ", STR_TO_DATE('" + effectiveDateTimeString + "', '%Y-%m-%e %H:%i:%s'),'normal', 5, 1, 3)");
    logger.info("Create new encounter for patient with id " + pid + " on " + effectiveDateTimeString + ". New encounterId is " + encounterId);
}

/**
	Registers the new created encounter within the OpenEMR DB

	@param {DatabaseConnection} dbconn - the DB connection obj
	@param {Integer} pid - the patient Id
	@param {Integer} encounterId - the encounter Id
    @param {String} effectiveDateTimeString - the dateTime string when the observation was created
    @param {Integer} lastEncounterFormId - the encounter Id of the encounter before
*/
function registerEncounter(dbConn, pid, encounterId, effectiveDateTimeString, lastEncounterFormId) {
    dbConn.executeUpdate("INSERT INTO forms (date, encounter, form_name, form_id, pid, user, groupname, authorized, formdir) VALUES(STR_TO_DATE('" + effectiveDateTimeString + "', '%Y-%m-%e %H:%i:%s'), " + encounterId + ", 'New Patient Encounter', " + lastEncounterFormId + ",  " + pid + ", 'admin', 'Default', 1, 'newpatient')");
    logger.info("Register encounter form with id '" + encounterId + "' from " + effectiveDateTimeString + ". Patient Id was '" + pid + "'.");
}

/**
	Executes the vitals creation
	
	@param {DatabaseConnection} dbconn - the DB connection obj
	@param {Integer} pid - the patient Id
	@param {Integer} encounterId - the encounter Id
	@param {String} effectiveDateTimeString - the dateTime string when the observation was created
	@param {Integer} pulse - the patients pulse
	@param {String} steps - the patients steps
*/
function execVitals(dbConn, pid, encounterId, effectiveDateTimeString, pulse, steps) {
    createVitals(dbConn, pid, effectiveDateTimeString, pulse, steps);
    var lastVitalsFormId = getId(dbConn, "id", "form_vitals", "ORDER BY id DESC LIMIT 1;");
    registerVitals(dbConn, pid, encounterId, effectiveDateTimeString, lastVitalsFormId);
}

/**
	Inserts a new vitals obj into the OpenEMR DB

	@param {DatabaseConnection} dbconn - the DB connection obj
	@param {Integer} pid - the patient Id
	@param {Integer} encounterId - the encounter Id
	@param {String} effectiveDateTimeString - the dateTime string when the observation was created
*/
function createVitals(dbConn, pid, effectiveDateTimeString, pulse, steps) {
    dbConn.executeUpdate("INSERT INTO form_vitals (date, pid, user, groupname, activity, pulse, note) VALUES (STR_TO_DATE('" + effectiveDateTimeString + "', '%Y-%m-%e %H:%i:%s'), " + pid + ", 'admin', 'Default', 1, " + pulse + ", '" + steps + " Steps')");
    logger.info("Add new vitals with pulse(" + pulse + ") and steps(" + steps + ") for patient '" + pid + "' from " + effectiveDateTimeString);
}

/**
	Registers the vital data within the OpenEMR DB

	@param {DatabaseConnection} dbconn - the DB connection obj
	@param {Integer} pid - the patient Id
	@param {Integer} encounterId - the encounter Id
	@param {String} effectiveDateTimeString - the dateTime string when the observation was created
*/
function registerVitals(dbConn, pid, encounterId, effectiveDateTimeString, lastVitalsFormId) {
    // Vitals-Form registrieren
    dbConn.executeUpdate("INSERT INTO forms (date, encounter, form_name, form_id,pid,user,groupname, authorized,formdir) VALUES (STR_TO_DATE('" + effectiveDateTimeString + "', '%Y-%m-%e %H:%i:%s'), " + encounterId + ", 'Vitals', " + lastVitalsFormId + ", " + pid + ", 'admin', 'Default', 1, 'Vitals')");
    logger.info("Register vitals form with Id '" + lastVitalsFormId + "'. For patient '" + pid + "' with encounter '" + encounterId + "' from " + effectiveDateTimeString);
}

/**
	Create response for client interaction

	@param {JSON} fhirConfig - the fhirConfig containing relevant data for FHIR interaction
	
	@return {String} return the response message
*/
function writeResponse(fhirConfig) {
    var response;
    if (fhirConfig.preferReturn == 'minimal' || (!fhirConfig.preferReturn && fhirConfig.fhirType == 'binary')) {
        logger.info("Response return type was set to minimal");
        // If the Prefer header is set to minimal then don't send back the created resource
        response = FhirResponseFactory.getCreateResponse(fhirConfig.fhirId, fhirConfig.versionId, fhirConfig.lastUpdated, 201);
        if (fhirConfig.preferReturn == 'minimal') {
            response.addHeader('Preference-Applied', 'return=minimal');
        }
    } else {
        logger.info("Response return type was set to representation");
        response = FhirResponseFactory.getCreateResponse(fhirConfig.data, fhirConfig.fhirId, fhirConfig.versionId, fhirConfig.lastUpdated, 201, fhirConfig.contentType);
        if (fhirConfig.preferReturn == 'representation') {
            response.addHeader('Preference-Applied', 'return=representation');
        }
    }

    responseMap.put('response', response);
    return response.getMessage();
}